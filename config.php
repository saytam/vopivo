<?php

require 'db.php';

$employeeFileLocation = "resources/employees.csv";

// možné přidat do Employee.class.php -> souvisí to přímo s Employees a ne obecně, static final proměná modelu employee
$employeeTableColumns = ['name' => 'Name', 'surname' => 'Surname', 'gender' => 'Gender', 'email' => 'Email', 'position' => 'Position', 'boss' => 'Boss', 'action' => 'Action'];
