<?php

namespace cv4_du\Utils;

class Form {
    private string $formPrefix;
    private array $postData;

    public function __construct($formPrefix) {
        $this->formPrefix = $formPrefix;
    }

    public function printInput(string $name, string $title, bool $required, ?string $value = '') {
        $requiredTag = ' <span style="color: red">*</span>';

        echo '
            <label for="' . $this->formPrefix . '_' . $name . '">' . $title . ($required ? $requiredTag : null) . '</label>
            <input type="text" class="form-control" id="' . $this->formPrefix . '_' . $name . '" value="' . htmlspecialchars($value) . '" name="' . $this->formPrefix . '[' . $name . ']" ' . ($required ? "required" : null) . '>';
    }

    public function validatePostData(): bool {
        return (is_array($this->postData) && (count($this->postData) == 6 || count($this->postData) == 7));
    }

    //    Tady serverová validace - jestli máme name atp. ideálně validační class .. ValidatePostForm, ValidateEmployee..
    public function getPostData(): array {
        return $this->postData;
    }

    public function setPostData(array $postData): void {
        $this->postData = $postData;
    }


}