<?php

namespace cv4_du\Lib;

use cv4_du\Model\Employee;

class DataLoaderEmployee extends DataLoader {
    public function hydrate(array $row) {
        $employee = new Employee();
        $employee->setId($row['id']);
        $employee->setName($row['name']);
        $employee->setSurname($row['surname']);
        $employee->setGender($row['gender']);
        $employee->setEmail($row['email']);
        $employee->setPosition($row['position']);
        $employee->setBoss($row['boss']);
        $employee->setRole($row['role']);

        return $employee;
    }
}