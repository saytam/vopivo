<?php

namespace cv4_du\Lib;

interface DataLoaderInterface {

    public function fetchAllRows(?string $sort, ?int $offset);

    public function sorter(array $rows, ?string $sortedBy);

    public function hydrate(array $row);

}