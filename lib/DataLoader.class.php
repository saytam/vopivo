<?php

namespace cv4_du\Lib;

use PDO;

class DataLoader implements DataLoaderInterface {
    private \PDO $db;
    private string $modelClassName;
    private string $getter;
    private string $specificFilterValue;

    public function __construct(\PDO $db, string $modelClassName) {
        $this->db = $db;
        $this->modelClassName = $modelClassName;
    }

    public function fetchAllRows(?string $sort = '', ?int $offset = 0): array {
        $sqlQuery = 'SELECT * FROM employees';

        if (!empty($sort)) {
            $sqlQuery .= (' ORDER BY ' . $sort);
        }

        $sqlQuery .= (' LIMIT 10 OFFSET ' . $offset . ';');

//        ASK: následující query nefunguje s bindValue, proč?
//'SELECT * FROM employees ORDER BY ? LIMIT 10 OFFSET ?'
        $employeesQuery = $this->db->prepare($sqlQuery);
//        $employeesQuery->bindValue(1, $sort);
//        $employeesQuery->bindValue(2, $offset, PDO::PARAM_INT);
        $employeesQuery->execute();
        $employees = $employeesQuery->fetchAll(PDO::FETCH_ASSOC);
        $data = [];
        if (is_array($employees) && !empty($employees)) {
            foreach ($employees as $row) {
                if (is_array($row)) {
                    try {
                        $data[] = $this->hydrate($row);
                    } catch
                    (\Exception $e) {
//                      TODO handle error
                    }
                }
            }
        }
        return $data;
    }

    public function hydrate(array $row) {
        // TODO: Implement hydrate() method.
    }

    public function addRow(array $data): bool {
        try {
            $employeesQuery = $this->db->prepare('INSERT INTO employees (name, surname, gender, email, position, boss) VALUES (?, ?, ?, ?, ?, ?);');
            $employeesQuery->execute([
                $data['name'], $data['surname'], $data['gender'], $data['email'], $data['position'], $data['boss']
            ]);
            return true;
        } catch (\Exception $e) {
            return false;
        }
    }

    public function updateRow(array $data): bool {
        try {
            $employeesQuery = $this->db->prepare("UPDATE employees SET name=?, surname=?, gender=?, email=?, position=?, boss=? WHERE id=? LIMIT 1;");
            $employeesQuery->execute([
                $data['name'], $data['surname'], $data['gender'], $data['email'], $data['position'], $data['boss'], $data['id']
            ]);
            return true;
        } catch (\Exception $e) {
            return false;
        }
    }

    public function deleteRow(string $id): bool {
        try {
            $employeesQuery = $this->db->prepare('DELETE FROM employees WHERE id=?');
            $employeesQuery->execute([$id]);
            return true;
        } catch (\Exception $e) {
            return false;
        }
    }

    public function countEmployees(): ?int {
        try {
            $employeesQuery = $this->db->prepare("SELECT COUNT(id) FROM employees");
            $employeesQuery->execute();
            return $employeesQuery->fetchColumn();
        } catch (\Exception $e) {
            return null;
        }
    }

    public function fetchRow(string $id) {
        try {
            $employeesQuery = $this->db->prepare("SELECT * FROM employees WHERE id=?");
            $employeesQuery->execute([$id]);
            return $employeesQuery->fetchAll();
        } catch (\Exception $e) {
            return null;
        }
    }

    public function login(?string $id, ?string $password) {
        try {
            $employeesQuery = $this->db->prepare("SELECT * FROM employees WHERE login=? AND password=?");
            $employeesQuery->execute([$id, $password]);
            $response = $employeesQuery->fetchAll();
            $data = [];
            if (!empty($response)) {
                try {
                    $data[] = $this->hydrate($response[0]);
                } catch
                (\Exception $e) {
//                      TODO handle error
                }
            }

            if (isset($data[0])) {
                return $data[0];
            } else {
                return [];
            }
        } catch (\Exception $e) {
            return false;
        }
    }

    public function sorter(array $rows, ?string $sortedBy): array {
        if (!empty($sortedBy)) {
            $this::setGetter($this->camelize('get_' . $sortedBy));

            if (is_callable([new $this->modelClassName(), $this->getter])) {
                uasort($rows, function ($a, $b): int {
                    $str1 = $a->{$this->getter}();
                    $str2 = $b->{$this->getter}();

                    return strnatcmp(strtolower($str1), strtolower($str2));
                });
            } else {
//                TODO: Throw exception
            }
        } else {
//            TODO: Throw exception
        }
        return $rows;
    }

    public function setGetter(string $getter): void {
        $this->getter = $getter;
    }

    private function camelize($word, $delimiter = '_'): string {
        $elements = explode($delimiter, $word);

        for ($i = 0; $i < count($elements); $i++) {
            if (0 == $i) {
                $elements[$i] = strtolower($elements[$i]);
            } else {
                $elements[$i] = strtolower($elements[$i]);
                $elements[$i] = ucwords($elements[$i]);
            }
        }

        return implode('', $elements);
    }

    public function filter(array $rows, string $filteredBy, ?string $specificFilterValue): array {
        if (!empty($filteredBy)) {
            $this::setGetter($this->camelize('get_' . $filteredBy));
            $this::setSpecificFilterValue($specificFilterValue);

            if (is_callable([new $this->modelClassName(), $this->getter])) {
                if (!empty($specificFilterValue)) {
                    return array_filter($rows, function ($el) {
                        return $el->{$this->getter}() == $this->specificFilterValue;
                    });
                } else {
                    return array_filter($rows, function ($el) {
                        return (!is_null($el) || $el === false);
                    });
                }
            } else {
//                TODO: Throw exception
            }
        } else {
//            TODO: Throw exception
        }
    }

    public function setSpecificFilterValue(string $specificFilterValue): void {
        $this->specificFilterValue = $specificFilterValue;
    }


}