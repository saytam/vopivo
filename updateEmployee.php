<?php
require 'common.php';

use cv4_du\Utils\Form;

$employeeToUpdate = [];
if (isset($_GET['updateRowNumber']) && !empty($_GET['updateRowNumber'])) {
    $employeeToUpdate = $employeeLoader->fetchRow($_GET['updateRowNumber'])[0];
}

$employees = $employeeLoader->fetchAllRows();
$bosses = $employeeLoader->filter($employees, $employeeTableColumns['position'], 'mistr');
$addEmployeeForm = new Form('employee');
if (isset($_POST['employee'])) {
    $filteredPostData = array_map(function ($el) {
        if (!empty($el)) {
            return $el;
        }
        return null;
    }, $_POST['employee']);
    $addEmployeeForm->setPostData($filteredPostData);
    if ($addEmployeeForm->validatePostData()) {
        $isRowUpdated = $employeeLoader->updateRow($addEmployeeForm->getPostData());

        if ($isRowUpdated) {
            header('Location: employeeActionAlert.php?message=updated');
        } else {
            header('Location: employeeActionAlert.php?message=fail');
        }
    } else {
        header('Location: employeeActionAlert.php?message=fail');
    }
}
?>
<?php include 'templates/header.php' ?>
    <h1>Edit employee</h1>
    <a href="index.php" class="btn btn-primary"
       style="margin: 10px 0">Go back</a>
    <form method="post" style="width: 60%; margin: 40px 0;" class="mx-auto" action="">
        <input type="number" name="employee[id]" id="id" style="display: none"
               value="<?= $employeeToUpdate['id'] ?? null ?>"/>
        <div class="form-group">
            <?php $addEmployeeForm->printInput('name', $employeeTableColumns['name'], true, $employeeToUpdate['name'] ?? null) ?>
        </div>
        <div class="form-group">
            <?php $addEmployeeForm->printInput('surname', $employeeTableColumns['surname'], true, $employeeToUpdate['surname'] ?? null) ?>
        </div>
        <div class="form-group">
            <label for="employee_gender">Gender</label>
            <select id="employee_gender" name="employee[gender]" class="custom-select">
                <option <?= !empty(($employeeToUpdate['gender'] ?? null)) ? ' selected="selected"' : ''; ?>
                        value></option>
                <option value="m" <?= ($employeeToUpdate['gender'] ?? null) == 'm' ? ' selected="selected"' : ''; ?>>
                    Man
                </option>
                <option value="w" <?= ($employeeToUpdate['gender'] ?? null) == 'w' ? ' selected="selected"' : ''; ?>>
                    Woman
                </option>
            </select>
        </div>
        <div class="form-group">
            <?php $addEmployeeForm->printInput('email', $employeeTableColumns['email'], false, $employeeToUpdate['email'] ?? null) ?>
        </div>
        <div class="form-group">
            <?php $addEmployeeForm->printInput('position', $employeeTableColumns['position'], false, $employeeToUpdate['position'] ?? null) ?>
        </div>
        <div class="form-group">
            <label for="employee_boss">Boss</label>
            <select name="employee[boss]" id="employee_boss" class="custom-select">
                <option <?= !empty($employeeToUpdate['gender'] ?? null) ? ' selected="selected"' : ''; ?>></option>
                <?php
                foreach ($bosses as $boss) {
                    ?>
                    <option value="<?= $boss->getId() ?>" <?= $employeeToUpdate['boss'] == $boss->getId() ? ' selected="selected"' : ''; ?>>
                        <?= $boss->getName() . ' ' . $boss->getSurname() ?>
                    </option>
                    <?php
                }
                ?>
            </select>
        </div>

        <button type="submit" class="btn btn-primary">Submit</button>
    </form>

<?php include 'templates/footer.php' ?>