<?php
require 'loginImportSetup.php';

use cv4_du\Utils\Form;

$loginForm = new Form('login');

$errors = '';
if (!empty($_POST)) {

    $login = $_POST['login']['login'] ?? null;
    $password = $_POST['login']['password'] ?? null;
    if (empty($login) || empty($password)) {
        $errors .= 'You need to fill in both, id and password';
    }

    $loggedInEmployee = $employeeLoader->login($login, $password);
    if (empty($loggedInEmployee)) {
        $errors .= 'ID or password is incorrect';
    }

    if (empty($errors)) {
        $_SESSION['user'] = $_POST['login']['login'];
        $_SESSION['role'] = $loggedInEmployee->getRole();
        var_dump($_SESSION);
        header('Location: index.php');
    }
}

?>

<?php include 'templates/header.php' ?>
<h1>Login</h1>
<p>You need to sign in to use the app.</p>
<?php
if (!empty($errors)) {
    echo '<div style="color:red;">' . $errors . '</div>';
}
?>
<form method="post" style="width: 40%; margin: 40px 0;" class="mx-auto" action="">
    <div class="form-group">
        <?php $loginForm->printInput('login', 'ID', true) ?>
    </div>
    <div class="form-group">
        <?php $loginForm->printInput('password', 'Password', true) ?>
    </div>


    <button type="submit" class="btn btn-primary">Submit</button>
</form>

<?php include 'templates/footer.php' ?>
