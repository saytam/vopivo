<?php include 'templates/header.php' ?>
<?php
if ($_GET['message'] == 'success') {
    echo
    '<div class="alert alert-success" role="alert">
            New employee was added :)
        </div>';
} else if ($_GET['message'] == 'updated') {
    echo
    '<div class="alert alert-success" role="alert">
            Employee updated :)
        </div>';
} else {
    echo
    '<div class="alert alert-danger" role="alert">Something failed</div>';
}
?>
<div>
    <button type="reset" class="btn btn-primary"
            style="margin: 10px 0; display: inline-block"><a href="index.php"
                                                             style="width: 100%; height: 100%; color: white; text-decoration: none">Go
            to
            table</a>
    </button>
    <button type="reset" class="btn btn-primary"
            style="margin: 10px 0; display: inline-block"><a href="addEmployee.php"
                                                             style="width: 100%; height: 100%; color: white; text-decoration: none">Add
            another employee</a>
    </button>
</div>
<?php include 'templates/footer.php' ?>
