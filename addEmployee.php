<?php
require 'common.php';
require 'adminRequired.php';

use cv4_du\Utils\Form;


$employees = $employeeLoader->fetchAllRows();
$bosses = $employeeLoader->filter($employees, $employeeTableColumns['position'], 'mistr');
$addEmployeeForm = new Form('employee');
if (isset($_POST['employee'])) {
    $filteredPostData = array_map(function ($el) {
        if (!empty($el)) {
            return $el;
        }
        return null;
    }, $_POST['employee']);
    $addEmployeeForm->setPostData($filteredPostData);
    if ($addEmployeeForm->validatePostData()) {
        $isRowAdded = $employeeLoader->addRow($addEmployeeForm->getPostData());

        if ($isRowAdded) {
            header('Location: employeeActionAlert.php?message=success');
        } else {
            header('Location: employeeActionAlert.php?message=fail');
        }
    }
}
?>
<?php include 'templates/header.php' ?>
    <h1>Add new employee</h1>
    <a href="index.php" class="btn btn-primary"
       style="margin: 10px 0">Go back</a>
    <form method="post" style="width: 60%; margin: 40px 0;" class="mx-auto" action="">
        <div class="form-group">
            <?php $addEmployeeForm->printInput('name', $employeeTableColumns['name'], true) ?>
        </div>
        <div class="form-group">
            <?php $addEmployeeForm->printInput('surname', $employeeTableColumns['surname'], true) ?>
        </div>
        <div class="form-group">
            <label for="employee_gender">Gender</label>
            <select id="employee_gender" name="employee[gender]" class="custom-select">
                <option selected value></option>
                <option value="m">Man</option>
                <option value="w">Woman</option>
            </select>
        </div>
        <div class="form-group">
            <?php $addEmployeeForm->printInput('email', $employeeTableColumns['email'], false) ?>
        </div>
        <div class="form-group">
            <?php $addEmployeeForm->printInput('position', $employeeTableColumns['position'], false) ?>
        </div>
        <div class="form-group">
            <label for="employee_boss">Boss</label>
            <select name="employee[boss]" id="employee_boss" class="custom-select">
                <option selected></option>
                <?php
                foreach ($bosses as $boss) {
                    ?>
                    <option value="<?= $boss->getId() ?>">
                        <?= $boss->getName() . ' ' . $boss->getSurname() ?>
                    </option>
                    <?php
                }
                ?>
            </select>
        </div>

        <button type="submit" class="btn btn-primary">Submit</button>
    </form>

<?php include 'templates/footer.php' ?>