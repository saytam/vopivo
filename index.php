<?php
require 'common.php';

$count = $employeeLoader->countEmployees();

$sorter = null;
$offset = 0;
if (isset($_GET['sorter']) && !empty($_GET['sorter'])) {
    $sorter = $_GET['sorter'];
}

if (isset($_GET['offset']) && !empty($_GET['offset'])) {
    $offset = $_GET['offset'];
}

if (isset($_POST['deleteRowNumber']) && !empty($_POST['deleteRowNumber']) && $_SESSION['role'] == 1) {
    if ($employeeLoader->deleteRow($_POST['deleteRowNumber'])) {
        header('Location: index.php');
    }
}

$employees = $employeeLoader->fetchAllRows($sorter, $offset);
?>

<?php include 'templates/header.php' ?>

    <h1 style="margin-bottom: 20px">Employee management service</h1>
    <div>
        <?php
        if ($_SESSION['role'] == 1) {
            echo '
            <a href="addEmployee.php" class="btn btn-primary" style="margin: 10px 0; display: inline-block">
                Add new employee
            </a>  
            ';
        }
        ?>
        <form method="get" action="" style="display: inline-block; width: 250px; margin-left: 8px">
            <select name="sorter" id="sorter" class="custom-select" onchange='this.form.submit()'>
                <option selected value>Select attribute to sort by</option>
                <?php
                foreach ($employeeTableColumns as $key => $sorterOption) {
                    ?>
                    <option <?= ($sorter == $key ? 'selected' : null) ?> value="<?= $key ?>">
                        <?= $sorterOption ?>
                    </option>
                    <?php
                }
                ?>
            </select>
        </form>
    </div>
    <table class="table" style="margin: 40px 0;">
        <thead>
        <tr>
            <?php
            foreach ($employeeTableColumns as $title) {
                ?>
                <th scope="col"><?php echo $title ?></th>
                <?php
            }
            ?>
        </tr>
        </thead>
        <tbody>
        <?php
        $i = 0;
        foreach ($employees as $employee) {
            ?>
            <tr>
                <td><?= htmlspecialchars($employee->getName()); ?></td>
                <td><?= htmlspecialchars($employee->getSurname()); ?></td>
                <td><?= htmlspecialchars($employee->getGender()); ?></td>
                <td><?= htmlspecialchars($employee->getEmail()); ?></td>
                <td><?= htmlspecialchars($employee->getPosition()); ?></td>
                <td><?= htmlspecialchars($employee->getBoss()); ?></td>

                <td class="center">
                    <form method="post">
                        <?php
                        if ($_SESSION['role'] == 1) {
                            echo '
                        <button type="submit" id="deleteRowNumber" name="deleteRowNumber"
                                value="<?= $employee->getId(); ?>"
                                class="btn btn-link">Delete
                        </button>
                        ';
                        }
                        ?>
                    </form>
                    <a href="updateEmployee.php?updateRowNumber=<?= $employee->getId(); ?>" class="btn btn-link">
                        Update
                    </a>
                </td>
            </tr>
            <?php
        }
        ?>
        </tbody>
    </table>

    <nav aria-label="pagination navigation">
        <ul class="pagination pagination-sm">
            <?php
            for ($i = 1; $i <= ceil($count / 10); $i++) {
                echo '<li class="page-item ' . ($offset / 10 + 1 == $i ? 'disabled' : '') . '">
<!--                                ASK: Jak dynamicky přidat druhej parametr do query? -->
                <a class="page-link" href="index.php?offset=' . (($i - 1) * 10) . '" tabindex="-1">' . $i . '</a>
            </li>';
            }
            ?>
        </ul>
    </nav>

<?php include 'templates/footer.php' ?>