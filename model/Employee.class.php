<?php

namespace cv4_du\Model;

class Employee {
    private int $id;
    private string $name;
    private string $surname;
    private ?string $gender;
    private ?string $email;
    private ?string $position;
    private ?int $boss;
    private ?int $role = 3;

    public function getRole(): ?int {
        return $this->role;
    }

    public function setRole(?int $role): void {
        $this->role = $role;
    }

    public function getId(): int {
        return $this->id;
    }

    public function setId(int $id): void {
        $this->id = $id;
    }

    public function getName(): string {
        return $this->name;
    }

    public function setName(string $name): void {
        $this->name = $name;
    }

    public function getSurname(): string {
        return $this->surname;
    }

    public function setSurname(string $surname): void {
        $this->surname = $surname;
    }

    public function getGender(): ?string {
        return $this->gender;
    }

    public function setGender(?string $gender): void {
        $this->gender = $gender;
    }

    public function getEmail(): ?string {
        return $this->email;
    }

    public function setEmail(?string $email): void {
        $this->email = $email;
    }

    public function getPosition(): ?string {
        return $this->position;
    }

    public function setPosition(?string $position): void {
        $this->position = $position;
    }

    public function getBoss(): ?int {
        return $this->boss;
    }

    public function setBoss(?int $boss): void {
        $this->boss = $boss;
    }


}