<?php

$validated = (isset($_SESSION['role']) && $_SESSION['role'] == 1);

if (!$validated) {
    header('HTTP/1.0 401 Unauthorized');
    die ("Unauthorized");
}