<?php

require 'config.php';
require_once 'model/Employee.class.php';
require_once 'lib/DataLoader.interface.php';
require_once 'lib/DataLoader.class.php';
require_once 'lib/DataLoaderEmployee.class.php';
require_once 'utils/Form.class.php';

use cv4_du\Lib\DataLoaderEmployee;

$employeeLoader = new DataLoaderEmployee($db, 'cv4_du\Model\Employee');

session_start();

if (empty($_SESSION['user'])) {
    header('Location: login.php');
}
