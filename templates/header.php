<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css"
          integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    <title>Employees App</title>
</head>
<body>
<div class="container" style="margin-top: 20px;">
<?php
if (isset($_SESSION['user'])) {
    echo '
         <div style="position: absolute; top: 10px; right: 10px;">' .
        'Signed in as ' . '<strong>' . $_SESSION['user'] . ' </strong > ' . ' | ' . '<a href = "logout.php" > Logout</a>'
        . '</div>';
}
?>